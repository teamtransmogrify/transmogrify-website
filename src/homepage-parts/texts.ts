export const before_video = `Transmogrify is a unique mix of puzzle solving, challenging combat, and fiendish platforming`
export const after_video = `Escape from a mysterious facility using brains, brawn, and help from tons of weapons and powerups`
export const after_videos_bullets = [
  `Transmogrify strange creatures to solve puzzles and help you advance`,
  `Challenging and tight platforming`,
  `Art style that is a perfect blend of creepy and cute`,
  `Fly, push, grab, rocket jump, and glide: powerups allow you to decide how to move through levels`,
  `Over 75 unique levels across 4 different worlds`,
]
