const GOOGLE_ANALYTICS_ID = "UA-119252109-7"

module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: `Transmogrify`,
    url: `https://playtransmogrify.com`,
    description: `An unusual 2D puzzle platformer that blends challenging action and handcrafted puzzles. Transmogrify monsters into puzzle solving tools with your peculiar gun, and escape the infested facility!`,
    author: `Transmogrify`,
    image: "/Cover_HD.jpg",
    bugReportUrl: `http://transmogrifybugtrackerapi-env.eba-sskchnuh.us-east-1.elasticbeanstalk.com/bug`,
    bugTrackerUrl: ``,
    userLogLocations: {
      windows: `%appdata%\\Godot\\app_userdata\\Transmogrify`,
      apple: `$HOME/.godot/Transmogrify`,
      linux: `$HOME/.godot/Transmogrify`,
    },
    authors: [
      { name: "Andrew Strauch", email: "andrew@onanodyssey.com", twitter: "thetransmogrify" },
      { name: "Jad Sarout", email: "jad@onanodyssey.com", twitter: "xananax" },
    ],
  },
  plugins: [
    `gatsby-plugin-emotion`,
    `gatsby-image`,
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-remark-images`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/content/blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      extensions: [`.mdx`, `.md`],
      plugins: [`gatsby-remark-images`],
      options: {
        defaultLayouts: {
          default: require.resolve(`./src/components/Layout.tsx`),
        },
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          {
            resolve: `gatsby-remark-autolink-headers`,
            options: {
              icon: false,
              offsetY: 350,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-plugin-google-tagmanager",
      options: {
        id: GOOGLE_ANALYTICS_ID,
        includeInDevelopment: false,
        gtmAuth: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_AUTH_STRING",
        gtmPreview: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_PREVIEW_NAME",
        dataLayerName: "YOUR_DATA_LAYER_NAME",

        // Name of the event that is triggered
        // on every Gatsby route change.
        //
        // Defaults to gatsby-route-change
        routeChangeEventName: "YOUR_ROUTE_CHANGE_EVENT_NAME",
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: GOOGLE_ANALYTICS_ID,
        head: false,
        anonymize: true,
        respectDNT: true,
        // Enables Google Optimize using your container Id
        optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
        // Enables Google Optimize Experiment ID
        experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
        // Set Variation ID. 0 for original 1,2,3....
        variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
